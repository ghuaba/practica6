package controlador.ed.lista.exception;

public class TopeException extends Exception {

    public TopeException() {
        super("La pila esta llena");
    }
    
    public TopeException(String msg) {
        super(msg);
    }
}
