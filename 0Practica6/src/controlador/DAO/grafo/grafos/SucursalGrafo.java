package controlador.DAO.grafo.grafos;

import controlador.DAO.grafo.SucursalDao;
import controlador.DAO.grafo.EnvioDao;
import controlador.ed.cola.ColaI;
import controlador.ed.grafo.Adycencia;

import controlador.ed.grafo.GrafoEtiquetadoD;
import controlador.ed.grafo.exception.GrafoSizeExeption;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.NodoLista;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;
import java.util.HashMap;
import java.util.Map;

import modelo.Sucursal;
import modelo.Envio;

public class SucursalGrafo {

    private GrafoEtiquetadoD<Sucursal> grafo;
    private ListaEnlazada<Sucursal> lista = new ListaEnlazada<>();

    public SucursalGrafo() {
        SucursalDao cbd = new SucursalDao();
        lista = cbd.listar();
        grafo = new GrafoEtiquetadoD<>(lista.size());
        try {
            for (int i = 0; i < lista.size(); i++) {
                grafo.etiquetarVertice(i + 1, lista.get(i));
                System.out.println(lista.get(i));
            }
            System.out.println("----------------");
            llenarGrafo();
        } catch (Exception e) {
        }
    }

    public ListaEnlazada<Sucursal> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Sucursal> lista) {
        this.lista = lista;
    }

    public GrafoEtiquetadoD<Sucursal> getGrafo() {
        return grafo;
    }

    public void setGrafo(GrafoEtiquetadoD<Sucursal> grafo) {
        this.grafo = grafo;
    }

    private void llenarGrafo() {
        try {
            for (int i = 0; i < lista.size(); i++) {
                String nombreOrigen = lista.get(i).getNombre();
                HashMap<String, Double> mapa = new HashMap<>();
                System.out.println("Sucursal Origen: " + nombreOrigen);
                ListaEnlazada<Envio> listaVuelos = new EnvioDao().listaPorSucursal(nombreOrigen);
                for (int j = 0; j < listaVuelos.size(); j++) {
                    Envio vuelo = listaVuelos.get(j);
                    String nombreDestino = vuelo.getDestino();
                    double duracion = vuelo.getValor();

                    if (mapa.containsKey(nombreDestino)) {
                        double suma = mapa.get(nombreDestino) + duracion;
                        mapa.put(nombreDestino, suma);
                    } else {
                        mapa.put(nombreDestino, duracion);
                    }
                }

                for (Map.Entry<String, Double> entry : mapa.entrySet()) {
                    String nombreDestino = entry.getKey();
                    double duracion = entry.getValue();

                    // Obtener nombres de las sucursales
                    Sucursal sucursalOrigen = getSucursal(nombreOrigen);
                    Sucursal sucursalDestino = getSucursal(nombreDestino);
                    grafo.insertarAristaE(sucursalOrigen, sucursalDestino, duracion);
                }
            }
        } catch (Exception e) {
            // Manejar la excepción apropiadamente
        }
    }

    private Sucursal getSucursal(String nombre) throws VacioException, PosicionException {
        Sucursal aux = null;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getNombre().equals(nombre)) {
                aux = lista.get(i);
                break;
            }
        }
        return aux;
    }

    
    
    
    
    public ListaEnlazada<Sucursal> bellmanFord(Sucursal origen, Sucursal destino) throws VacioException, PosicionException {
        int numVertices = grafo.numVertices();
        int[] distancias = new int[numVertices + 1];
        int[] padres = new int[numVertices + 1];

        // Inicializar distancias y padres
        for (int i = 1; i <= numVertices; i++) {
            distancias[i] = Integer.MAX_VALUE;
            padres[i] = -1;
        }
        distancias[grafo.getVerticeNum((Sucursal) origen)] = 0;

        // Algoritmo de Bellman-Ford
        for (int i = 1; i <= numVertices - 1; i++) {
            for (int u = 1; u <= numVertices; u++) {
                ListaEnlazada<Adycencia> adyacentes = grafo.adycentes(u);
                for (int j = 0; j < adyacentes.size(); j++) {
                    Adycencia ady = adyacentes.get(j);
                    int v = ady.getDestino();
                    double pesoUV = ady.getPeso();
                    if (distancias[u] != Integer.MAX_VALUE && distancias[u] + pesoUV < distancias[v]) {
                        distancias[v] = (int) (distancias[u] + pesoUV);
                        padres[v] = u;
                    }
                }
            }
        }

        // Construir el camino más corto desde el destino hasta el origen
        ListaEnlazada<Sucursal> camino = new ListaEnlazada<>();
        int actual = grafo.getVerticeNum((Sucursal) destino);

        if (origen == destino) {

            throw new RuntimeException("No existe un camino entre el origen y el destino.");

        }
        if (distancias[actual] == Integer.MAX_VALUE) {
            throw new RuntimeException("No existe un camino entre el origen y el destino.");
        }
        while (actual != -1) {
            camino.insertarInicio((Sucursal) grafo.getEtiqueta(actual));
            actual = padres[actual];
        }

        return camino;
    }
    
    
    

    private ListaEnlazada<Adycencia> obtenerAdyacentesGE(Sucursal vertice) throws GrafoSizeExeption {
        return grafo.adyacentesGE(vertice);
    }

    public boolean existeCaminoBFS(Sucursal cuentaOrigen, Sucursal cuentaDestino) throws VacioException, PosicionException, TopeException, GrafoSizeExeption {
        GrafoEtiquetadoD<Sucursal> grafo = getGrafo();
        if (grafo.getVerticeNum(cuentaOrigen) == null || grafo.getVerticeNum(cuentaDestino) == null) {
            return false;
        }
        Integer cuentaOrigenInt = grafo.getVerticeNum(cuentaOrigen);
        Integer cuentaDestinoInt = grafo.getVerticeNum(cuentaDestino);
        // Búsqueda en amplitud para encontrar una conexión entre las cuentas
        ColaI<Integer> queue = new ColaI<>(grafo.numVertices());
        ListaEnlazada<Integer> visitados = new ListaEnlazada<>();
        queue.queue(cuentaOrigenInt);
        visitados.insertar(cuentaOrigenInt);
        while (!queue.isEmpty()) {
            Integer nodoActual = queue.dequeue();
            if (nodoActual.equals(cuentaDestinoInt)) {
                return true;
            }
            ListaEnlazada<Adycencia> adyacentes = obtenerAdyacentesGE(getSucursal(nodoActual));
            NodoLista<Adycencia> aux = adyacentes.getCabecera();
            while (aux != null) {
                Integer vecino = aux.getInfo().getDestino();
                if (!visitados.contiene(vecino)) {
                    queue.queue(vecino);
                    visitados.insertar(vecino);
                }
                aux = aux.getSig();
            }
        }
        return false;
    }
    
        private Sucursal getSucursal(int vertice) {
        return grafo.getEtiqueta(vertice);
    }

}
