package modelo;

public class Sucursal {

    private Integer id;
    private String nombre;
    private String nro_cuenta;

    public Sucursal() {
    }

    public Sucursal(Integer id, String nombre, String nro_cuenta) {
        this.id = id;
        this.nombre = nombre;
        this.nro_cuenta = nro_cuenta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNro_cuenta() {
        return nro_cuenta;
    }

    public void setNro_cuenta(String nro_cuenta) {
        this.nro_cuenta = nro_cuenta;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
