package controlador.ed.grafo.exception;

public class GrafoSizeExeption extends Exception {

    public GrafoSizeExeption(String msg) {
        super(msg);
    }
    public GrafoSizeExeption() {
        super("Accedio a un tamanio fuera del grafo");
    }
}








