package vista.grafo;

import controlador.DAO.grafo.SucursalDao;
import controlador.DAO.grafo.EnvioDao;
import controlador.DAO.grafo.grafos.SucursalGrafo;
import javax.swing.JOptionPane;

import vista.FrmGrafo;
import vista.modeloTabla.ModeloTablaEnvios;
import vista.modeloTabla.ModeloTablaSucursal;

public class FrmSucursalPrincipal extends javax.swing.JDialog {

    private ModeloTablaSucursal modeloSucursal = new ModeloTablaSucursal();
    private ModeloTablaEnvios modeloEnvio = new ModeloTablaEnvios();
    private SucursalGrafo sg = new SucursalGrafo();
    private SucursalDao sdao = new SucursalDao();
  

    public FrmSucursalPrincipal(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cargarTablaCuenta();

    }


    
   
    private void mostrarGrafo() {
        new FrmGrafo(null, true, sg.getGrafo()).setVisible(true);
    }

    private void cargarTablaCuenta() {
        modeloSucursal.setLista(sdao.listar());
        tbltablaC.setModel(modeloSucursal);
        tbltablaC.updateUI();

    }

    private void limpiar() {
        txtNombreSucursal.setText("");

    }

    private void guardar() {
        if (txtNombreSucursal.getText().isEmpty()  /*txtNroCuenta.getText().isEmpty()*/) {
            JOptionPane.showMessageDialog(null, "Llene todos los campos", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                sdao.getCb().setNombre(txtNombreSucursal.getText());
                sdao.guardar();
                sdao.setCb(null);
                cargarTablaCuenta();
                limpiar();
                JOptionPane.showMessageDialog(null, "Guardado correctamente", "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
            }
        }

    }


 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbltablaC = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombreSucursal = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        btnSearchCamino = new javax.swing.JButton();
        btnIngresarDistancias = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Lista de sucursales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 24))); // NOI18N

        tbltablaC.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbltablaC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbltablaCMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbltablaC);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(520, 40, 360, 400);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar sucursales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 24))); // NOI18N
        jPanel5.setLayout(null);

        jLabel1.setText("Ciudad Sucursal:");
        jPanel5.add(jLabel1);
        jLabel1.setBounds(20, 50, 140, 30);
        jPanel5.add(txtNombreSucursal);
        txtNombreSucursal.setBounds(60, 90, 139, 26);

        jButton1.setText("Ver grafo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton1);
        jButton1.setBounds(300, 30, 100, 40);

        jButton4.setText("Cancelar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton4);
        jButton4.setBounds(160, 140, 110, 26);

        jButton5.setText("Guardar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton5);
        jButton5.setBounds(290, 140, 100, 26);

        jPanel1.add(jPanel5);
        jPanel5.setBounds(40, 50, 420, 180);

        btnSearchCamino.setText("Ingresar Distancias");
        btnSearchCamino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchCaminoActionPerformed(evt);
            }
        });
        jPanel1.add(btnSearchCamino);
        btnSearchCamino.setBounds(270, 270, 180, 50);

        btnIngresarDistancias.setText("Buscar Caminos");
        btnIngresarDistancias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarDistanciasActionPerformed(evt);
            }
        });
        jPanel1.add(btnIngresarDistancias);
        btnIngresarDistancias.setBounds(30, 270, 200, 50);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 940, 490);

        setSize(new java.awt.Dimension(901, 488));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tbltablaCMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbltablaCMouseClicked
        // TODO add your handling code here:
      
    }//GEN-LAST:event_tbltablaCMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        mostrarGrafo();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        limpiar();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void btnSearchCaminoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchCaminoActionPerformed
        this.dispose();

        new FrmDistancia(null, true).setVisible(true);
                
                

// TODO add your handling code here:
    }//GEN-LAST:event_btnSearchCaminoActionPerformed

    private void btnIngresarDistanciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarDistanciasActionPerformed
  this.dispose();

        new FrmBusquedas(null, true).setVisible(true);
               


        // TODO add your handling code here:
    }//GEN-LAST:event_btnIngresarDistanciasActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmSucursalPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmSucursalPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmSucursalPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmSucursalPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create ancbxod display the dialogcbxd */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmSucursalPrincipal dialog = new FrmSucursalPrincipal(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIngresarDistancias;
    private javax.swing.JButton btnSearchCamino;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbltablaC;
    private javax.swing.JTextField txtNombreSucursal;
    // End of variables declaration//GEN-END:variables
}
