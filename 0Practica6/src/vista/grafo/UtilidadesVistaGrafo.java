package vista.grafo;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import javax.swing.JComboBox;
import modelo.Sucursal;

public class UtilidadesVistaGrafo {

    public static void cargarCombo(ListaEnlazada<Sucursal> lista, JComboBox cbx)
            throws VacioException, PosicionException {
        cbx.removeAllItems();
        for (int i = 0; i < lista.size(); i++) {
            cbx.addItem(lista.get(i));
        }
    }

    public static Sucursal obtenerCombo(ListaEnlazada<Sucursal> lista, JComboBox cbx)
            throws VacioException, PosicionException {
        return lista.get(cbx.getSelectedIndex());
    }
}
