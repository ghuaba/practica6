package controlador.ed.grafo;

import controlador.ed.grafo.exception.GrafoSizeExeption;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;

public abstract class Grafo {

    public abstract Integer numVertices();

    public abstract Integer numAristas();

    public abstract Boolean existeArista(Integer i, Integer j) throws GrafoSizeExeption;

    public abstract Double pesoArista(Integer i, Integer j) throws GrafoSizeExeption;

    public abstract void insertar(Integer i, Integer j) throws GrafoSizeExeption;

    public abstract void insertar(Integer i, Integer j, Double peso) throws GrafoSizeExeption;

    public abstract ListaEnlazada<Adycencia> adycentes(Integer i);

    
       
   public boolean estaConectado() throws VacioException, PosicionException {
    if (numVertices() == 0) {
        return false; // Si no hay vértices, el grafo no está conectado
    }

    ListaEnlazada<Integer> visitados = new ListaEnlazada<>();
    ListaEnlazada<Integer> resultado = busquedaEnProfundidad(1); // Realiza búsqueda en profundidad desde el primer vértice

    return resultado.size() == numVertices(); // Si la cantidad de vértices visitados es igual al total de vértices, el grafo está conectado
}
    
    // Método para realizar la búsqueda en profundidad
public ListaEnlazada<Integer> busquedaEnProfundidad(Integer inicio) throws VacioException, PosicionException {
    ListaEnlazada<Integer> resultado = new ListaEnlazada<>();
    boolean[] visitados = new boolean[numVertices() + 1];
    busquedaEnProfundidadRecursiva(inicio, visitados, resultado);
    return resultado;
}

private void busquedaEnProfundidadRecursiva(Integer vertice, boolean[] visitados, ListaEnlazada<Integer> resultado) throws VacioException, PosicionException {
    visitados[vertice] = true;
    resultado.insertar(vertice);

    ListaEnlazada<Adycencia> adycentes = adycentes(vertice);
    for (int i = 0; i < adycentes.size(); i++) {
        Adycencia ady = adycentes.get(i);
        Integer destino = ady.getDestino();
        if (!visitados[destino]) {
            busquedaEnProfundidadRecursiva(destino, visitados, resultado);
        }
    }
}
    
    
    
    
    
    
    @Override
    public String toString() {
        StringBuilder grafo = new StringBuilder("GRAFO" + "\n");
        for (int i = 1; i <= numVertices(); i++) {
            grafo.append(" V " + i + "\n");
            ListaEnlazada<Adycencia> lista = adycentes(i);
            grafo.append((!lista.isEmpty()) ? "Adycencias" : "No Adycencias");
            grafo.append("\n");
            for (int j = 0; j < lista.size(); j++) {
                try {
                    Adycencia aux = lista.get(j);
                    grafo.append(" -- V " + aux.getDestino() + " PESO --> " + aux.getPeso() + "\n");
                } catch (Exception e) {
                }
            }
        }
        return grafo.toString();
    }

    
}
