package controlador.ed.grafo;

import controlador.ed.grafo.exception.GrafoSizeExeption;
import controlador.ed.lista.ListaEnlazada;

public class GrafoD extends Grafo {

    protected Integer numV;
    protected Integer numA;
    private Double[][] distancia;
    protected ListaEnlazada<Adycencia> listaAdycencia[];

    public GrafoD(Integer nroVertices) {
        numV = nroVertices;
        numA = 0;
        listaAdycencia = new ListaEnlazada[nroVertices + 1];
        for (int i = 1; i <= nroVertices; i++) {
            listaAdycencia[i] = new ListaEnlazada<>();
        }
    }

    @Override
    public Integer numVertices() {
        return numV;
    }

    @Override
    public Integer numAristas() {
        return numA;
    }

    @Override
    public Boolean existeArista(Integer i, Integer j) throws GrafoSizeExeption {
        Boolean esta = false;
        if (i.intValue() <= numV && j.intValue() <= numV) {
            ListaEnlazada<Adycencia> lista = listaAdycencia[i];
            for (int k = 0; k < lista.size(); k++) {
                try {
                    Adycencia aux = lista.get(k);
                    if (aux.getDestino().intValue() == j.intValue()) {
                        esta = true;
                        break;
                    }
                } catch (Exception e) {
                }
            }
        } else {
            throw new GrafoSizeExeption();
        }
        return esta;
    }

    @Override
    public Double pesoArista(Integer i, Integer j) throws GrafoSizeExeption {
        Double esta = Double.NaN;
        if (i.intValue() <= numV && j.intValue() <= numV) {
            ListaEnlazada<Adycencia> lista = listaAdycencia[i];
            for (int k = 0; k < lista.size(); k++) {
                try {
                    Adycencia aux = lista.get(k);
                    if (aux.getDestino().intValue() == j.intValue()) {
                        esta = aux.getPeso();
                        break;
                    }
                } catch (Exception e) {
                }
            }
        } else {
            throw new GrafoSizeExeption();
        }
        return esta;
    }

    @Override
    public void insertar(Integer i, Integer j) throws GrafoSizeExeption {
        insertar(i, j, Double.NaN);
    }

    @Override
    public void insertar(Integer i, Integer j, Double peso) throws GrafoSizeExeption {
        if (i.intValue() <= numV
                && j.intValue() <= numV) {
            if (!existeArista(i, j)) {
                listaAdycencia[i].insertar(new Adycencia(j, peso));
                numA++;
            }
        } else {
            throw new GrafoSizeExeption();
        }
    }

    @Override
    public ListaEnlazada<Adycencia> adycentes(Integer i) {
        return listaAdycencia[i];
    }

//FLOYD
    public void algoritmoFloyd() {
        try {
            distancia = new Double[numV + 1][numV + 1];
            for (int i = 1; i <= numV; i++) {
                for (int j = 1; j <= numV; j++) {
                    if (i == j) {
                        distancia[i][j] = 0.0;
                    } else if (existeArista(i, j)) {
                        distancia[i][j] = pesoArista(i, j);
                    } else {
                        distancia[i][j] = Double.POSITIVE_INFINITY;
                    }
                }
            }
            for (int k = 1; k <= numV; k++) {
                for (int i = 1; i <= numV; i++) {
                    for (int j = 1; j <= numV; j++) {
                        if (distancia[i][k] + distancia[k][j] < distancia[i][j]) {
                            distancia[i][j] = distancia[i][k] + distancia[k][j];
                        }
                    }
                }
            }
            for (int k = 1; k <= numV; k++) {
                for (int i = 1; i <= numV; i++) {
                    for (int j = 1; j <= numV; j++) {
                        if (distancia[i][k] + distancia[k][j] < distancia[i][j]) {
                            distancia[i][j] = distancia[i][k] + distancia[k][j];
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ListaEnlazada<Integer> caminoFloyd(int origen, int destino) throws Exception {
        //Primero ejecuta el algoritmo floyd
        algoritmoFloyd();
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        if (distancia[origen][destino] == Double.POSITIVE_INFINITY) {
            return camino;
        }
        int verticeActual = destino;
        camino.insertarInicio(verticeActual);
        while (verticeActual != origen) {
            for (int i = 1; i <= numV; i++) {
                if (distancia[origen][i] + pesoArista(i, verticeActual) == distancia[origen][verticeActual]) {
                    verticeActual = i;
                    camino.insertarInicio(verticeActual);
                    break;
                }
            }
        }
        return camino;
    }

    public ListaEnlazada<Double> distanciaVertice(Integer inicio) throws Exception {
        //Primero ejecuta el algoritmo floyd
        algoritmoFloyd();
        ListaEnlazada<Double> listaDistancias = new ListaEnlazada<>();
        listaDistancias.insertar(0.0);
        for (int i = 1; i <= numV; i++) {
            if (i != inicio) {
                listaDistancias.insertar(distancia[inicio][i]);
            }
        }
        return listaDistancias;
    }

    public Double obtenerDistanciaFloyd(Integer o, Integer d) throws Exception {
        algoritmoFloyd();
        return distancia[o][d];
    }
}
