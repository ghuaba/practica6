package controlador;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilidades {
    public static String formatDate(Date fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return formato.format(fecha);
    }
}
