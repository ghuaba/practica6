package controlador.DAO.grafo;

import controlador.DAO.AdaptadorDao;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import java.io.IOException;
import modelo.Sucursal;

public class SucursalDao extends AdaptadorDao<Sucursal> {

    private Sucursal cb;

    public SucursalDao() {
        super(Sucursal.class);
    }

    public Sucursal getCb() {
        if (cb == null) {
            cb = new Sucursal();
        }
        return cb;
    }

    public void setCb(Sucursal cb) {
        this.cb = cb;
    }

    public void guardar() throws IOException {
        cb.setId(generateId());
        this.guardar(cb);
    }

    public void modificar(Integer pos) throws IOException, VacioException, PosicionException {

        this.modificar(cb, pos);
    }

    private Integer generateId() {
        return listar().size() + 1;
    }

    public Sucursal buscarSucursal(String nroCuenta) throws VacioException, PosicionException {
        Sucursal sucursal = null;
        ListaEnlazada<Sucursal> lista = listar();
        for (int i = 0; i < lista.size(); i++) {
            Sucursal aux = lista.get(i);
            if (aux.getNro_cuenta().equalsIgnoreCase(nroCuenta)) {
                sucursal = aux;
                break;
            }
        }
        return sucursal;
    }

}
