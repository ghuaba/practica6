package vista.modeloTabla;

import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Sucursal;

public class ModeloTablaSucursal extends AbstractTableModel {
    private ListaEnlazada<Sucursal> lista = new ListaEnlazada<>();

    public ListaEnlazada<Sucursal> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Sucursal> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Sucursal c = null;
        try {
            c = lista.get(i);
        } catch (Exception e) {
        }
        switch(i1){
            case 0: return c.getId();
            case 1: return c.getNombre();
//            case 1: return c.getNro_cuenta();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0: return "Id";
            case 1: return "Nombre";
//            case 2: return "Cuenta";
            default: return null;
        }
    }
    
    
    
}
