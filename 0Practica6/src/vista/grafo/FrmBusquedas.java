/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package vista.grafo;

import controlador.DAO.grafo.grafos.SucursalGrafo;
import controlador.ed.lista.ListaEnlazada;
import javax.swing.JOptionPane;
import modelo.Sucursal;
import vista.FrmGrafo;

/**
 *
 * @author ghuaba
 */
public class FrmBusquedas extends javax.swing.JDialog {

    private SucursalGrafo sg = new SucursalGrafo();

    
    public FrmBusquedas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        loadCombos();
    }

    private void loadCombos() {
        try {
            UtilidadesVistaGrafo.cargarCombo(sg.getLista(), cbxBusquedaO);
            UtilidadesVistaGrafo.cargarCombo(sg.getLista(), cbxBusquedaD);

        } catch (Exception e) {
        }

    }

    private void ejecutarFloyd() {
        try {
            Sucursal cuentaOrigen = UtilidadesVistaGrafo.obtenerCombo(sg.getLista(), cbxBusquedaO);
            Sucursal cuentaDestino = UtilidadesVistaGrafo.obtenerCombo(sg.getLista(), cbxBusquedaD);
            boolean existeCamino = sg.existeCaminoBFS(cuentaOrigen, cuentaDestino);
            if (existeCamino) {
                long startTime = System.currentTimeMillis(); // Iniciar medición de tiempo

                ListaEnlazada<Integer> caminoIndices = sg.getGrafo().caminoFloyd(sg.getGrafo().getVerticeNum(cuentaOrigen), sg.getGrafo().getVerticeNum(cuentaDestino));

                long endTime = System.currentTimeMillis(); // Finalizar medición de tiempo
                long totalTime = endTime - startTime; // Calcular tiempo total en milisegundos

                ListaEnlazada<Sucursal> caminoDistancia = new ListaEnlazada<>();
                for (int i = 0; i < caminoIndices.size(); i++) {
                    int indice = caminoIndices.get(i);
                    caminoDistancia.insertar(sg.getGrafo().getEtiqueta(indice));
                }

                StringBuilder caminoStr = new StringBuilder();
                for (int i = 0; i < caminoDistancia.size(); i++) {
                    Sucursal aeropuerto = caminoDistancia.get(i);
                    caminoStr.append(aeropuerto.getNombre());
                    if (i < caminoDistancia.size() - 1) {
                        caminoStr.append(" -> ");
                    }
                }
                txtCamino.setText("Camino mínimo (Floyd):\n" + caminoStr.toString());
                txtCamino.append("\nTiempo de ejecución: " + totalTime + " ms");
            } else {

                JOptionPane.showMessageDialog(
                        null,
                        "No existe camino entre las cuentas seleccionadas.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE
                );
                txtCamino.setText("");
            }

        } catch (Exception e) {
            txtCamino.setText("No se pudo encontrar un camino mínimo");
            e.printStackTrace();
        }
    }

    private void ejecutarBellman() {
        try {
            Sucursal cuentaOrigen = UtilidadesVistaGrafo.obtenerCombo(sg.getLista(), cbxBusquedaO);
            Sucursal cuentaDestino = UtilidadesVistaGrafo.obtenerCombo(sg.getLista(), cbxBusquedaD);
            boolean existeCamino = sg.existeCaminoBFS(cuentaOrigen, cuentaDestino);
            if (existeCamino) {
                long startTime = System.currentTimeMillis(); // Iniciar medición de tiempo

                ListaEnlazada<Sucursal> camino = sg.bellmanFord(cuentaOrigen, cuentaDestino);

                long endTime = System.currentTimeMillis(); // Finalizar medición de tiempo
                long totalTime = endTime - startTime; // Calcular tiempo total en milisegundos

                StringBuilder caminoStr = new StringBuilder();
                for (int i = 0; i < camino.size(); i++) {
                    Sucursal aeropuerto = camino.get(i);
                    caminoStr.append(aeropuerto.getNombre());
                    if (i < camino.size() - 1) {
                        caminoStr.append(" -> ");
                    }
                }
                txtCamino.setText("Camino mínimo (Bellman-Ford):\n" + caminoStr.toString());
                txtCamino.append("\nTiempo de ejecución: " + totalTime + " ms");
            } else {
                JOptionPane.showMessageDialog(
                        null,
                        "No existe camino entre las cuentas seleccionadas.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE
                );
                txtCamino.setText("");
            }

        } catch (Exception e) {
            txtCamino.setText("No se pudo encontrar un camino mínimo");
            e.printStackTrace();
        }
    }

    
    //no lo uso pero en el examen talvez
    private void caminoBellman() {
        try {
            ListaEnlazada lista = sg.bellmanFord(UtilidadesVistaGrafo.obtenerCombo(sg.getLista(), cbxBusquedaO),
                    UtilidadesVistaGrafo.obtenerCombo(sg.getLista(), cbxBusquedaD));
            lista.imprimir();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No existe un camino para esta ruta", "Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    private void mostrarGrafo() {
        new FrmGrafo(null, true, sg.getGrafo()).setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cbxBusquedaO = new javax.swing.JComboBox<>();
        cbxBusquedaD = new javax.swing.JComboBox<>();
        btnFloyd = new javax.swing.JButton();
        btnBellman = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtCamino = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Busqueda", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 24))); // NOI18N
        jPanel4.setLayout(null);

        jLabel3.setText("Origen");
        jPanel4.add(jLabel3);
        jLabel3.setBounds(50, 40, 70, 20);

        jLabel4.setText("Destino");
        jPanel4.add(jLabel4);
        jLabel4.setBounds(50, 90, 60, 20);

        cbxBusquedaO.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel4.add(cbxBusquedaO);
        cbxBusquedaO.setBounds(170, 30, 120, 26);

        cbxBusquedaD.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel4.add(cbxBusquedaD);
        cbxBusquedaD.setBounds(170, 90, 120, 26);

        btnFloyd.setText("Floyd");
        btnFloyd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFloydActionPerformed(evt);
            }
        });
        jPanel4.add(btnFloyd);
        btnFloyd.setBounds(30, 170, 110, 30);

        btnBellman.setText("Bellman-Ford");
        btnBellman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBellmanActionPerformed(evt);
            }
        });
        jPanel4.add(btnBellman);
        btnBellman.setBounds(170, 170, 120, 30);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(50, 30, 320, 230);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Camino", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 24))); // NOI18N

        txtCamino.setEditable(false);
        txtCamino.setColumns(20);
        txtCamino.setRows(5);
        jScrollPane3.setViewportView(txtCamino);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel7);
        jPanel7.setBounds(380, 130, 520, 160);

        jButton1.setText("Ver grafo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(140, 280, 140, 50);

        jButton2.setText("Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(20, 370, 110, 30);

        jButton3.setText("IngresarDistancia");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(230, 380, 150, 26);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 910, 460);

        setSize(new java.awt.Dimension(916, 488));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnFloydActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFloydActionPerformed
        // TODO add your handling code here:
        ejecutarFloyd();
    }//GEN-LAST:event_btnFloydActionPerformed

    private void btnBellmanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBellmanActionPerformed


        ejecutarBellman();
            
    }//GEN-LAST:event_btnBellmanActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        mostrarGrafo();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();

        new FrmSucursalPrincipal(null, true).setVisible(true);

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        this.dispose();

        new FrmDistancia(null, true).setVisible(true);

    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmBusquedas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmBusquedas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmBusquedas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmBusquedas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmBusquedas dialog = new FrmBusquedas(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBellman;
    private javax.swing.JButton btnFloyd;
    private javax.swing.JComboBox<String> cbxBusquedaD;
    private javax.swing.JComboBox<String> cbxBusquedaO;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea txtCamino;
    // End of variables declaration//GEN-END:variables
}
