package controlador.DAO.grafo;

import controlador.DAO.AdaptadorDao;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import java.io.IOException;
import modelo.Envio;

public class EnvioDao extends AdaptadorDao<Envio> {

    private Envio e;

    public EnvioDao() {
        super(Envio.class);
    }

    public Envio getE() {
        if (e == null) {
            e = new Envio();
        }
        return e;
    }

    public void setE(Envio e) {
        this.e = e;
    }

    public void guardar() throws IOException, VacioException, PosicionException {
        e.setId(generateId());
        this.guardar(e);
    }

    public void modificar(Integer pos) throws IOException, VacioException, PosicionException {
        this.modificar(e, pos);
    }

    private Integer generateId() {
        return listar().size() + 1;
    }

    public ListaEnlazada<Envio> listaPorSucursal(String nombreSucursal) throws VacioException, PosicionException {
        ListaEnlazada<Envio> lista = new ListaEnlazada<>();
        ListaEnlazada<Envio> listado = listar();
        for (int i = 0; i < listado.size(); i++) {
            Envio aux = listado.get(i);
            if (aux.getNro_cuenta().equals(nombreSucursal)) {
                lista.insertar(aux);
            }
        }
        return lista;
    }


}
