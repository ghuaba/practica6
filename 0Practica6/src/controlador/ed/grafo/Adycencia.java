package controlador.ed.grafo;

public class Adycencia {

    private Integer destino;
    private double peso;

    public Adycencia() {
    }

    public Adycencia(Integer destino, double peso) {
        this.destino = destino;
        this.peso = peso;
    }

    public Integer getDestino() {
        return destino;
    }

    public void setDestino(Integer destino) {
        this.destino = destino;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

}
