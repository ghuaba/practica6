package modelo;

import java.util.Date;

public class Envio {

    private Integer id;
//    private String origen;
    private String nro_cuenta;//Quien me hizo el envio de la gasolina

    private String destino;

    private Date fecha;
    private Double valor;
    private Integer id_cuenta;//la cuenta dueña de la trasaccion

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

//    public String getOrigen() {
//        return origen;
//    }
//
//    public void setOrigen(String origen) {
//        this.origen = origen;
//    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getNro_cuenta() {
        return nro_cuenta;
    }

    public void setNro_cuenta(String nro_cuenta) {
        this.nro_cuenta = nro_cuenta;
    }

    public Integer getId_cuenta() {
        return id_cuenta;
    }

    public void setId_cuenta(Integer id_cuenta) {
        this.id_cuenta = id_cuenta;
    }
    
    
    

}
