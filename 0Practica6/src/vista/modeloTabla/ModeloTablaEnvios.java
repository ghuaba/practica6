package vista.modeloTabla;

import controlador.DAO.grafo.SucursalDao;
import controlador.Utilidades;
import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Envio;
/**
 *
 * @author ghuaba
 */
public class ModeloTablaEnvios extends AbstractTableModel {
    private ListaEnlazada<Envio> lista = new ListaEnlazada<>();

    public ListaEnlazada<Envio> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Envio> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Envio ev = null;
                SucursalDao cbd = new SucursalDao();

        try {
            ev = lista.get(i);
                        cbd.setCb(cbd.buscarSucursal(ev.getNro_cuenta()));

        } catch (Exception e) {
        }
        switch(i1){
            case 0: return ev.getId();
            case 1: return ev.getNro_cuenta();
            case 2: return ev.getDestino();
            case 3: return Utilidades.formatDate(ev.getFecha());
            case 4: return ev.getValor();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0: return "Id";
            case 1: return "Origen";
            case 2: return "Destino";
            case 3: return "Fecha";
            case 4: return "Envio";
            default: return null;
        }
    }
    
    
    
}
