package controlador.exception;

public class EspacioException extends Exception {

    public EspacioException(String message) {
        super(message);
    }
    
    public EspacioException() {
        super("No hay espacio suficiente o posicion no valida");
    }
    
}
